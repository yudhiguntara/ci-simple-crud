/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : pos

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2017-01-30 12:07:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `category_name` varchar(40) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Furniture');
INSERT INTO `categories` VALUES ('2', 'Food & Drink');
INSERT INTO `categories` VALUES ('3', 'Office Tools');
INSERT INTO `categories` VALUES ('4', 'Electronics');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sku` varchar(10) default NULL,
  `name` varchar(60) default NULL,
  `description` text,
  `barcode` varchar(20) default NULL,
  `price` decimal(10,2) default NULL,
  `tax` varchar(255) default NULL,
  `created_at` timestamp NULL default NULL,
  `updated_at` timestamp NULL default NULL on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `sku` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'IKEA-369', 'IKEA Linnmon / Adils Meja', 'Terdapat lubang untuk kaki agar mudah dipasang. Kaki yang dapat diatur membuat meja berdiri stabil juga pada lantai yang tidak rata. Harga jual kami sama dengan Toko Asli loh.', null, '439000.00', '5', '2017-01-30 05:53:29', '2017-01-30 11:53:29');
INSERT INTO `products` VALUES ('2', 'FNK-97', 'Funika Meja Komputer ', 'Meja Komputer dengan Rak Buku merupakan meja komputer 2in1 berbahan engineered E1 particle board yang kuat, tahan lama, tidak berbau, dan tidak mudah melengkung. Meja ini didesain simple, minimalis, dan stylish dengan kompartemen yang dapat dijadikan sebagai rak buku pada bagian sisi kanannya, sehingga buku Anda dapat tersusun lebih rapi. Meja ini ideal diletakkan pada ruang kerja atau ruang belajar anak Anda.', null, '908000.00', '5', '2017-01-30 08:59:51', null);
INSERT INTO `products` VALUES ('3', 'IKEA-476', 'IKEA Ekby Laiva Rak', 'Harga jual kami sama dengan Toko Asli.', null, '39900.00', '5', '2017-01-30 09:01:28', '2017-01-30 11:55:34');
INSERT INTO `products` VALUES ('4', 'IKEA-30', 'IKEA Lack Rak', 'Rak menjadi satu dengan dinding berkat alat pasang yang tersembunyi. Harga jual kami sama dengan Toko Asli.', null, '189000.00', '10', '2017-01-30 11:09:28', '2017-01-30 11:09:30');
INSERT INTO `products` VALUES ('6', 'IKEA-22', 'IKEA Ekby Hemnes Rak', 'Terbuat dari kayu padat yaitu bahan alami yang tahan lama Paket tidak termasuk siku/braket dan baut Harga jual kami sama dengan Toko Asli.', null, '199000.00', '5', '2017-01-30 05:32:42', null);

-- ----------------------------
-- Table structure for product_categories
-- ----------------------------
DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` tinyint(3) unsigned NOT NULL,
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `product_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_categories_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_categories
-- ----------------------------
INSERT INTO `product_categories` VALUES ('1', '1');
INSERT INTO `product_categories` VALUES ('2', '1');
INSERT INTO `product_categories` VALUES ('3', '1');
