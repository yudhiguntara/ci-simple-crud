<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
	}

	public function index()
	{
		$data['products'] = $this->product_model->get_products();
		$this->load->view('product/list', $data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sku', 'stock keeping unit', 'trim|required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('name', 'product\'s name', 'trim|required|max_length[60]');
		$this->form_validation->set_rules('description', 'description', 'trim');
		$this->form_validation->set_rules('price', 'price', 'trim|numeric');
		$this->form_validation->set_rules('tax', 'tax', 'trim|numeric');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() === TRUE) {
			$new_product = array(
				'sku' => $this->input->post('sku'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
				'price' => $this->input->post('price'),
				'tax' => $this->input->post('tax'),
				'created_at' => date('Y-m-d H:i:s')
			);

			if ($this->product_model->add_product($new_product))
				$this->session->set_flashdata('success', 'New product has been added');
			else
				$this->session->set_flashdata('error', 'An error occured');
		} else
			$this->session->set_flashdata('error', $this->form_validation->error_string('',''));

		$data['categories'] = $this->product_model->get_categories();
		$this->load->view('product/new', $data);
	}

	public function edit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('product_id', 'id', 'trim|required');
		$this->form_validation->set_rules('sku', 'stock keeping unit', 'trim|required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('name', 'product\'s name', 'trim|required|max_length[60]');
		$this->form_validation->set_rules('description', 'description', 'trim');
		$this->form_validation->set_rules('price', 'price', 'trim|numeric');
		$this->form_validation->set_rules('tax', 'tax', 'trim|numeric');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() === TRUE) {
			$product_id = $this->input->post('product_id');
			$product = array(
				'sku' => $this->input->post('sku'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
				'price' => $this->input->post('price'),
				'tax' => $this->input->post('tax')
			);

			if ($this->product_model->update_product($product_id, $product))
				$this->session->set_flashdata('success', 'Product has been updated');
			else
				$this->session->set_flashdata('error', 'An error occured');
		} else
			$this->session->set_flashdata('error', $this->form_validation->error_string('',''));
		
		$product_id = $this->uri->segment(3) ? $this->uri->segment(3, true) : null;
		$this->product_model->product_id = $product_id;
		$product = $this->product_model->get_products();
		if ($product_id && $product->num_rows() > 0) {
			$data['product'] = $product->row();
			$data['categories'] = $this->product_model->get_categories();
			$this->load->view('product/edit', $data);
		}
	}

	function delete(){
		$product_id = $this->uri->segment(3) ? $this->uri->segment(3, true) : null;
		$this->product_model->product_id = $product_id;
		if ($this->product_model->remove_product())
			$this->session->set_flashdata('del_success', 'Product has been deleted');
		else
			$this->session->set_flashdata('del_error', 'An error occured');
		
		redirect('product');
	}

	function masukin_data(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sku', 'stock keeping unit', 'trim|required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('name', 'product\'s name', 'trim|required|max_length[60]');
		$this->form_validation->set_rules('description', 'description', 'trim');
		$this->form_validation->set_rules('price', 'price', 'trim|numeric');
		$this->form_validation->set_rules('tax', 'tax', 'trim|numeric');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() === TRUE) {
			$new_product = array(
				'sku' => $this->input->post('sku'),
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
				'price' => $this->input->post('price'),
				'tax' => $this->input->post('tax'),
				'created_at' => date('Y-m-d H:i:s')
			);

			if ($this->product_model->add_product($new_product))
				echo 'New product has been added';
			else
				echo 'An error occured';
		} else
			echo $this->form_validation->error_string();

	}

}

/* End of file Product.php */
/* Location: ./application/controllers/Product.php */